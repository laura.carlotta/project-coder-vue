const Contador = {
    name: 'contador',
    data() {
        return {
            num: 0,
            eixoX: 0,
            eixoY: 0,
            eixoX1: 0,
            eixoY1: 0
        }
    },
    methods: {
        contador(passo, event) {
            console.log(this.num, event);
            return this.num += passo;
        },

        mouseCheckFocus(event) {
            this.eixoX = event.clientX;
            this.eixoY = event.clientY;
        },

        mouseCheckFocus1(event) {
            this.eixoX1 = event.clientX;
            this.eixoY1 = event.clientY;
        },

        stoped( event ) {
            event.stopPropagation();
        },
        
        enterMethodDown(event) {
            if (event.keyCode == 13) {
                console.log('Apertei', event);
            }
        },

        enterMethodUp(event) {
            if (event.keyCode == 13) {
                console.log('Soltei', event);
            }
        },

        alertUser(event) {
            alert('Você tem certeza que deseja enviar o formulário?');
        }
    }
}

Vue.createApp(Contador).mount('#app')