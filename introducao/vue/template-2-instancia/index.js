const Titulo = {
    name: 'titulo',
    data() {
        return {
            titulo: '123 já'
        }
    },
    methods: {
        alterarTitulo(event) {
            this.titulo = event.target.value;
        }
	}
}

Vue.createApp(Titulo).mount('#app')
