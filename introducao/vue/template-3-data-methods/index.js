const Titulo = {
    name: 'titulo',

    data() {
        return {
            titulo: '123 já',
			link: 'http://google.com.br',
        }
    },
    methods: {
        alterarTitulo(event) {
            this.titulo = event.target.value;
        }
	}
}

Vue.createApp(Titulo).mount('#app')
