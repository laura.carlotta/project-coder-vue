const Titulo = {
    name: 'titulo',
    data() {
        return {
            titulo: '123 já',
			link: 'http://google.com.br',
        }
    },

	create: {
		mostrarInstanciaCriadaNoConsole() {
			let instanciaCriada = document.querySelector('[testeLaura-bind]');
			console.log(instanciaCriada);
		}
	},

    methods: {
        alterarTitulo(event) {
            this.titulo = event.target.value;
        },
	},
}

Vue.createApp(Titulo).mount('#app')
