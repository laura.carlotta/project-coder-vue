const Contador = {
    name: 'contador',
    data() {
        return {
            num: 0,
			eixoX: 0,
			eixoY: 0
        }
    },
    methods: {
        addition( anyNumber, event ) {
			console.log(anyNumber, event);
            return this.num += anyNumber;
        },

		mouseCheckFocus(event) {
			this.eixoX  = event.clientX;
			this.eixoY  = event.clientY;
		}
	}
}

Vue.createApp(Contador).mount('#app')
