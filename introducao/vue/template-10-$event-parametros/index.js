const Contador = {
    name: 'contador',
    data() {
        return {
            num: 0,
            eixoX: 0,
            eixoY: 0
        }
    },
    methods: {
        contador(passo, event) {
            console.log(this.num, event);
            return this.num += passo;
        },

        mouseCheckFocus(event) {
            this.eixoX = event.clientX;
            this.eixoY = event.clientY;
        }
    }
}

Vue.createApp(Contador).mount('#app')