const Contador = {
    name: 'contador',
    data() {
        return {
            num: 0
        }
    },
    methods: {
        contador() {
            return this.num ++;
        }
	}
}

Vue.createApp(Contador).mount('#app')
