const Titulo = {
    name: 'titulo',
    data() {
        return {
            titulo: '123 já',
			link: 'http://google.com.br',
			linkDressAndGo: '<em><a href="http://dressandgo.com.br/">Dress And Go</a></em>'
        }
    },

    methods: {
        copiaTitulo() {
            this.titulo = 'Bom dia, flor do dia!!';
			return this.titulo;
        },
	},
}

Vue.createApp(Titulo).mount('#app')
