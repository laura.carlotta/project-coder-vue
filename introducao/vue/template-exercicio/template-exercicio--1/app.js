const Infos = {
	name: 'Info',

	data() {
		return {
			name: 'Laura',
			age: 28,
			img: 'https://viagemeturismo.abril.com.br/wp-content/uploads/2016/10/ilhas-faroe-arquipelago-na-dinamarca-thinkstock.jpeg?quality=70&strip=info&w=928'
		}
	},

	methods: {
		ageX3() {
			return this.age * 3;
		},

		randomNumber() {
			return (Math.random()).toFixed(3);
		}
	}
}

Vue.createApp(Infos).mount('#challange')
