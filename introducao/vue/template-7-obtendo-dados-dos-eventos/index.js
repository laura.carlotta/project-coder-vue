const Contador = {
    name: 'contador',
    data() {
        return {
            num: 0,
        }
    },
    methods: {
        contador( event ) {
			console.log(event);
            return this.num ++;
        }
	}
}

Vue.createApp(Contador).mount('#app')
