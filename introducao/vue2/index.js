new Vue({
    el: '#app', // elemento quer aplicar/controlar a partir da instancia do vue
    data: { // sobre o template que é cotrolado pelo vue
        titulo: 'Usando vue'
    },
    methods: {
        alterarTitulo(event) {
            this.titulo = event.target.value;
        }
    }
});
